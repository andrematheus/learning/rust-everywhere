package com.example.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        System.loadLibrary("CoreAndroid")

        setContentView(R.layout.activity_main)

        val textView = findViewById<TextView>(R.id.textview)
        val result = "${hello(1.0, 2.0)}"
        textView.text = result
        android.util.Log.d(
            "RustEverywhere",
            "onCreate: hello(1.0, 2.0)=${hello(1.0, 2.0)}"
        )
    }

    external fun hello(value_a: Double, value_b: Double): Double
}