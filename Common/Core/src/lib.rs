pub struct Core {
    value_a: f64,
    value_b: f64
}

impl Core {
    pub fn new(value_a: f64, value_b: f64) -> Core {
        return Core {
            value_a,
            value_b
        };
    }

    pub fn add(&self) -> f64 {
        return self.value_a + self.value_b;
    }
}